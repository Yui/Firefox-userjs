#!/bin/sh

printf "\033[0;32mDeploying updates to codeberg...\033[0m\n"
# Clean Personal Details
[ -f clean ] && bash clean.sh

#Cleaner does this
#sed -i 's#WordToBeReplaced#ReplacedWithThisWord#g' file.txt
#End

read -p "Cleaned, enter to Push"
git add .
# Commit changes.
msg="Updated the overrides. $(date)"
if [ -n "$*" ]; then
	msg="$*"
fi
git commit -a -m "$msg"
# Push source and build repos.
git push
read -p "Press Enter to exit"