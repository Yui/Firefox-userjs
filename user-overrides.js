/**------------------------------------------------------------------------------------------**/
/**                                          Yui                                          */
/**--------------------------------------------------------------------------------------------/
		These are my custom user.js overrides for arkenfox user.js.
/--------------------------------------------------------------------------------------------**/

/**
 * Fix breakage
 */

user_pref("privacy.resistFingerprinting.letterboxing", false); // [SET] [!PRIV=true] whether to set the viewport size to a generic dimension in order to resist fingerprinting) - suggested to set this to 'true', however doing so may make the viewport smaller than the window
user_pref("media.peerconnection.enabled", true); // Enable WebRTC - For OpenH264 Video Codec(Videos on some sites need this.)
user_pref("network.http.referer.XOriginPolicy", 0) // Fix breakage of some sites(Example: twist.moe), Use ext instead: Smart Referer (Strict mode + add exceptions)

// WebGL
user_pref("webgl.disable-extensions", false); // [SET] [SAFE=false] [!PRIV=true] whether to enable WebGL extensions - 'true' may break Google Earth Street View on Google Maps
user_pref("webgl.disabled", false); // [SET] [SAFE=false] [!PRIV=true] whether to enable WebGL - `true` may break some sites (Google Earth Street View on Google Maps?) - WebGL appears to be dependent upon JS being enabled - can set to 'false' and control fingerprinting with CanvasBlocker
user_pref("webgl.dxgl.enabled", false); // [SET] [SAFE=true] [!PRIV=false] whether to enable WebGL (Windows) - is this DirectX graphics library? unknown
user_pref("webgl.enable-webgl2", true); // [SET] [SAFE=true] [!PRIV=false] whether to enable WebGL 2
user_pref("webgl.min_capability_mode", false); // [SET] [SAFE=false] [!PRIV=true] whether to restrict WebGL capabilities - 'true' breaks Google Earth Street View on Google Maps

user_pref("media.eme.enabled", false); // Enable DRM

/**
 * Setup DOH
 */

/**
user_pref("network.proxy.type", 5);											//Proxy -> Use system proxy settings
user_pref("network.trr.mode", 2);											// Dns over HTTPS  2- DoH, fall back to normal, 3- DoH only
user_pref("network.trr.bootstrapAddress", "CensoredIP"); 					// DNS server to use for resolving the DoH name
user_pref("network.trr.uri", "CensoredAddress");  							// DOH server addr
*/

/**
 * Re-enable wanted tools
 */

user_pref("devtools.chrome.enabled", true); // Enable developer tools

// Note: Use a privacy friendly search engine with this
user_pref("keyword.enabled", true); // Enable automatic search in URL bar
user_pref("browser.search.suggest.enabled", true); // Enable search suggestions
user_pref("browser.urlbar.suggest.searches", true); // Enable search suggestions

/**
 * Features
 */

// CSS Theming Support

//user_pref("svg.context-properties.content.enabled", true);			// Support for MaterialFox CSS Theme
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true); // allow userChrome/userContent

/**
 * QOL
 */
user_pref("browser.download.useDownloadDir", false); // Download to downloads dir by default
user_pref("browser.startup.page", 3); // Restore previous session (0=blank, 1=home, 2=last visited page, 3=resume previous session)
user_pref("privacy.clearOnShutdown.history", false); // Whether to clear history on shutdown
user_pref("privacy.clearOnShutdown.sessions", false); // Whether to clear Active Logins on shutdown - set to 'true' if sharing Firefox with another user

user_pref("_user.js.parrot", "Yui hacks loaded, everything is fine.");
/**------------------------------------------------------------------------------------------**/
/**                                        Yui End										 **/
/**------------------------------------------------------------------------------------------**/
